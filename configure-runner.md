## II. Configuring Runner
Trong GitLab, **Runner** thực hiện các công việc mà bạn xác định trong file `.gitlab-ci.yml`. Runner có thể là một máy ảo, một VPS, một máy vật lý cơ bản, một container docker hoặc thậm chí là một cụm container. GitLab và Runner giao tiếp thông qua API, vì vậy yêu cầu duy nhất là máy Runner có quyền truy cập Internet.

Máy Runner có thể dành riêng cho một dự án nhất định hoặc phục vụ nhiều dự án trong GitLab. Nếu nó phục vụ tất cả các dự án thì nó được gọi là **Shared Runner**.

Để tìm hiểu xem có Runner nào được chỉ định cho dự án của bạn hay không, truy cập 
**Settings ➔ CI/CD**. 

2 bước để Runner hoạt động:
1. Cài đặt Runner trên máy
2. Cấu hình máy Runner

## 1. Cài đặt Runner
> **Note**: Tài liệu này chỉ dành cho việc cài đặt và cấu hình Runner trên máy macOS thực hiện công việc build app iOS. Các nền tảng khác vui lòng tham khảo tại tại [Install GitLab Runner](https://docs.gitlab.com/runner/install/#install-gitlab-runner "Permalink")

GitLab Runner có thể được cài đặt và cập nhật trên macOS.
1.  Download the binary for your system:
    
    -   Đối với máy tính chạy CPU Intel:
        
        ``` bash
        sudo curl --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-amd64"
        ```
        
    -    Đối với máy tính chạy CPU Apple Silicon:
		    ```bash
			sudo curl --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-arm64"
			```
        
        
2.  Cấp quyền thực thi:
    
    ```bash
    sudo chmod +x /usr/local/bin/gitlab-runner
    ```

3. Chạy Runner:
Mở ứng dụng Terminal. Chuyển qua tài khoản người dùng hiện tại, thao tác trên giao diện hoặc gõ lệnh: 
	```bash
	su - <username>
	```
	Cài đặt GitLab Runner như 1 dịch vụ và khởi chạy nó:
	```bash
	gitlab-runner install
	gitlab-runner start
	```
4. Khởi động lại máy
5. (Optional) Check trạng thái của GitLab Runner bằng lệnh:
	```bash
	gitlab-runner status
	```
6. (Optional) Dừng dịch vụ:
	```bash
	gitlab-runner stop
	```

## 2. Cấu hình Runner

### 2.1. Shared, specific và group
Runner có thể được đăng ký dưới 3 loại là: shared, specific và group.

 - **Shared Runners**: hữu ích cho các công việc có yêu cầu tương tự nhau, sử dụng cho nhiều project trong nhóm hoặc trong cùng 1 phiên bản GitLab. Nó xử lý các công việc bằng cách dùng hành đợi sử dụng hợp lý ([fair usage queue](https://git.sohacorp.vn/help/ci/runners/README.md#how-shared-runners-pick-jobs)). Chỉ có thể đăng ký Shared Runners nếu có quyền truy cập quản trị viên vào phiên bản GitLab.
 - **Specific Runners**: rất hữu ích cho các công việc có yêu cầu đặc biệt hoặc cho các dự án có nhu cầu cụ thể. Thông thường, các Specific Runner được sử dụng cho một dự án tại một thời điểm. Việc sử dụng `tags` có thể hữu ích trong trường hợp này. Specific Runner xử lý công việc theo hàng đợi *FIFO*. 
 - **Group Runners**: sử dụng khi có nhiều dự án trong 1 nhóm và muốn tất cả dự án trong nhóm đều có thể truy cập vào 1 tập hợp các Runner. Group Runners xử lý công việc theo hàng đợi *FIFO*. 
 
Specific Runners có thể được thiết lập để nhiều dự án sử dụng. Sự khác biệt với Shared Runner là phải kích hoạt rõ ràng từng dự án để Runner có thể thực hiện các công việc của mình. Specific Runners không được chia sẻ tự động với các dự án forked. Forked repository sẽ sao chép cài đặt CI (jobs, allow shared, ...).

### 2.2. Đăng ký Specific Runner
Việc đăng ký một **Specific Runner** có thể được thực hiện theo hai cách:

 - Tạo Runner bằng mã token
 - Chuyển đổi Shared Runner sang Specific Runner (1 chiều, chỉ quyền admin)

#### Đăng ký Specific Runner bằng token
Để tạo một Specific Runner mà không có quyền quản trị đối với dự án, hãy truy cập dự án mà bạn muốn cho Runner hoạt động trong GitLab. Làm theo các bước sau đây

 1. Vào **Settings > CI/CD** để lấy được mã CI/CD token
 2. Mở terminal, chạy lệnh:
	```bash
	gitlab-runner register
	```
3. Nhập GitLab URL:
    
    -  Đối với Runner trên GitLab tự quản lý, sử dụng URL cho phiên bản GitLab đó. Ví dụ, nếu dự án lưu trữ ở địa chỉ `https://git.sohacorp.vn/tunguyenvan/Telegrams_5.1`, URL của phiên bản GitLab sẽ là `https://git.sohacorp.vn/`.
    - Đối với Runner trên GitLab.com, URL là `https://gitlab.com`.
  4. Nhập mã token đăng ký Runner
  5. Nhập tên cho Runner
  6. Nhập type thực thi cho Runner. Để xác định được máy Runner phù hợp với loại thực thi vào, tham khảo ở [Link](https://docs.gitlab.com/runner/executors/index.html). Ở đây đơn giản là nhập `shell` để có thể chạy các job bằng các lệnh.

Một cách tương tự để đăng ký Runner là sử dụng chế độ không tương tác ([non-interactive mode](https://docs.gitlab.com/runner/commands/index.html#non-interactive-registration)) khi cung cấp các tham số trên 1 lệnh đăng ký:
```bash
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "$RUNNER_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \  
  --tag-list  "docker,aws"  \  
  --run-untagged="true"  \  
  --locked="true"  \
  --description "docker-runner"
```

Để kiểm tra trạng thái đăng ký có thành công hay chưa, sử dụng lệnh `gitlab-runner status` hoặc kiểm tra trên trang URL của dự án. Vào **Settings > CI / CD > Runner Settings** để xem các Runner có trong dự án như hình dưới đây.
<!-- Chèn ảnh cap màn hình vào đây -->

<img src="./image/specific-runners.png"/>


#### Khóa Specific Runner không được kích hoạt cho các dự án khác
Một Specific Runner có thể được chỉ định chạy cho một dự án và không bị kích hoạt bởi 1 dự án khác. Cài đặt này có thể được thực hiện ngay tại bước đăng ký Runner bằng tham số `--locked="true"` và có thể được thay đổi sau đó trong cài đặt của mỗi Runner.

 1. Từ Project truy vập **Settings > CI/CD**
 2. Tìm Runner muốn lock/unlock và đảm bảo rằng nó được enable
 3. Bấm vào nút bút chì bên cạnh tên Runner
 4. Tích chọn vào **Lock to current projects**
 5. Bấm vào **Save changes** để những thay đổi có hiệu lực


#### Protected Runners
Runner có thể được cài đặt bảo vệ khỏi tiết lộ thông tin nhạy cảm. Bất cứ khi nào Runner ở trạng thái `protected`, Runner chỉ chọn các công việc được tạo trên các `protected branch` hoặc `protected tag` và bỏ qua các job khác. Các bước cài đặt như bên dưới.

 1. Từ Project truy vập **Settings > CI/CD**
 2. Tìm Runner muốn protected/unprotected và đảm bảo rằng nó được enable
 3. Bấm vào nút bút chì bên cạnh tên Runner
 4. Tích chọn vào **Protected**
 5. Bấm vào **Save changes** để những thay đổi có hiệu lực

<img src="./image/protected_runners.png"/>

### 2.2. Đăng ký Shared Runner và Group Runner
Để đăng ký Shared Runner và Group Runner, tham khảo thêm tại [Registering runners](https://docs.gitlab.com/runner/register/).


### 2.2. Cài đặt máy Runner chạy macOS thực hiện công việc build/deploy app iOS
Khi đã đăng ký máy Runner chạy trên macOS, bước tiếp theo là thiết lập cấu hình để build hoặc deploy app iOS. Các điều kiện cần đó là:
-   Tài khoán Apple Developer -  [https://developer.apple.com/](https://developer.apple.com/)
-   Ruby và công cụ dòng lệnh XCode được cài đặt trên máy Runner [https://docs.fastlane.tools/getting-started/ios/setup](https://docs.fastlane.tools/getting-started/ios/setup/)

Có 2 cách để build hoặc deploy app iOS bằng các lệnh Shell như sau:

1. Sử dụng công cụ dòng lệnh Xcode là `xcodebuild`
		
	 - Đọc tài liệu về `xcodebuild` bằng lệnh:
		```bash
		man xcodebuild
		```
	 - Lệnh build project :
		```bash
		xcodebuild clean -project ProjectName.xcodeproj -scheme SchemeName | xcpretty
		xcodebuild build -project ProjectName.xcodeproj -scheme SchemeName -destination 'platform=iOS Simulator,name=iPhone 8,OS=11.3' | xcpretty -s
		```
	 - Lệnh archive project :
		```bash
		xcodebuild clean archive -archivePath build/ProjectName -scheme SchemeName
		xcodebuild -exportArchive -exportFormat ipa -archivePath "build/ProjectName.xcarchive" -exportPath "build/ProjectName.ipa" -exportProvisioningProfile "ProvisioningProfileName"
		```

	Tham khảo thêm tài liệu [Building from the Command Line with Xcode FAQ](https://developer.apple.com/library/archive/technotes/tn2339/_index.html)


2. Sử dụng thư viện **fastlane**
Dự án Demo để tham khảo: [https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo)
		
	 - Cài đặt **fastlane**:
		 Tạo một file trong kho lưu trữ của dự án có tên là `Gemfile`, dán nội dung sau vào file:
		```bash
		source "https://rubygems.org"
		gem "fastlane"
		```
		Sau đó vào Terminal ở vị trí dự án, chạy lệnh: 
		```bash
		bundle install
		```
		
	- Khởi tạo **fastlane**:
		Chạy lệnh sau trên Terminal, chọn Option 2 cho target **Test Flight**:
		```bash
		bundle exec fastlane init
		```
		Sau đó nhập các thông tin như là tài khoản Apple Developer để hoàn thành đăng ký. Công cụ sẽ tạo sẵn folder `fastlane` với 2 file `Appfile` and `Fastfile`.
		
		![Initialize Fastlane](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/fastlane-init.png)
	- Khởi tạo **fastlane match**:
		Chạy lệnh và chọn Option 4.
		```mel
		bundle exec fastlane match init
		```
		![Initialize fastlane Match](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/match-init.png)
Để biết thêm thông tin về **fastlane match**, đọc thêm tài liệu [https://docs.fastlane.tools/actions/match/](https://docs.fastlane.tools/actions/match/)

	- Tạo **project access token**:
		Vào trong Profile của tài khoản GitLab để lấy được GitLab Access Token. Sau đó chạy lệnh sau để định cấu hình **fastlane** để sử dụng Access Token khi thực hiện các yêu cầu cho dự án:
		```bash
		export PRIVATE_TOKEN=YOUR_NEW_TOKEN
		```

	- Tạo **signing certificates**:
		Giờ đây **fastlane match** được cấu hình, chúng ta có thể sử dụng nó để tạo **signing certificates** và **provisioning profiles** cho ứng dụng và tải chúng lên GitLab.
		> **Note**: Nếu đã có những file này cho ứng dụng của mình, hãy xem hướng dẫn trong blog này về cách sử dụng **fastlane** để nhập các file: [/blog/2022/10/03/mobile-devops-with-gitlab-part-3-code-signing-for-ios-with-gitlab-and-fastlane/](https://about.gitlab.com/blog/2022/10/03/mobile-devops-with-gitlab-part-3-code-signing-for-ios-with-gitlab-and-fastlane/).

		Chạy lệnh sau từ Terminalđể tạo file **signing certificates** và upload lên GitLab:
		```mel
		bundle exec fastlane match development
		```
		Khi lệnh này hoàn tất, hãy truy cập **Settings > CI/CD** trong dự án và cuộn xuống phần **Secure Files** để xem các tệp vừa được tạo và thêm vào dự án.
		
		Làm tương tự với tạo file **Appstore code signing** bằng lệnh:
		```mel
		bundle exec fastlane match appstore
		```

	- Cập nhật cấu hình **Xcode**:
		Trên Project trên **Xcode**, vào **Signing & Capabilities** và disable **Automatically managing signing**. Sau đó chọn **Provisioning profile** và **Signing certificate** phù hợp..

		![Configure Xcode Provisioning Profiles](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/xcode.png)



	
⇒ Công việc còn lại là cấu hình file `.gitlab-ci.yml` để thực hiện các công việc với **fastlane**.

Ví dụ cấu hình file `.gitlab-ci.yml` chạy fastlane đơn giản: 
```bash
stages:
	- build
	- beta
cache:
	key:
		files:
			- Gemfile.lock
		paths:
			- vendor/bundle

build_ios:
	image: macos-13-xcode-14
	stage: build
	script:
		- bundle check --path vendor/bundle || bundle install --path vendor/bundle --jobs $(nproc)
		- bundle exec fastlane build
	tags: 
		- saas-macos-medium-m1

	beta_ios:
		image: macos-13-xcode-14
		stage: beta
		script:
			- bundle check --path vendor/bundle || bundle install --path vendor/bundle --jobs $(nproc)
			- bundle exec fastlane beta
		tags: 
			- saas-macos-medium-m1
		when:  manual
		allow_failure:  true
		only:
			refs:
				- main
```

> **Note**: Video hướng dẫn chi tiết tại: [Incubation Engineering - Mobile DevOps - Feb 2023 Showcase](https://www.youtube.com/watch?v=Ar8IsBgP1as)