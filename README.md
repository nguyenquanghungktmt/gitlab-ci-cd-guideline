# GitLab CI/CD Guideline for iOS Development
GitLab cung cấp dịch vụ Continuous Integration - CI (Tích hợp liên tục) và Continuous Deployment - CD (Triển khai liên tục), mục đích là với mỗi commit push vào 1 nhánh nhất định sẽ tạo 1 luồng (gọi là **pipeline**) thực hiện các công việc đã được cài đặt từ trước.

Các công việc cần thiết để có một luồng CI hoạt động bao gồm:
1. Thêm file `.gitlab-ci.yml` vào thư mục gốc của repository
File `.gitlab-ci.yml` đóng vai trò chỉ dẫn máy Runner các công việc phải làm. 

2. Cấu mình máy thực thi công việc, gọi là Runner
Máy Runner có thể là máy ảo, VPS, local computer, docker container, ... Máy Runner nhận và thực thi các yêu cầu định nghĩa trong file `.gitlab-ci.yml`, trả hoặc cỏ thể tải lên kết quả

> Note: Bắt đầu từ phiên bản 8.0, **GitLab Continuous Integration (CI)** được tích hợp hoàn toàn vào chính GitLab và được bật theo mặc định trên tất cả các dự án.
  
## Table of Contents
- [I. Create **.gitlab-ci.yml** file](#part-one)
- [II. Configuring Runner](#part-two)
- [Author](#author)
- [References](#References)
  

<a id="part-one"></a>
## I. Create `.gitlab-ci.yml` file

### 1. File `.gitlab-ci.yml` là gì
File `.gitlab-ci.yml` là nơi cấu hình những gì 1 pipeline CI thực hiện với dự án. Nó được đặt trong thư mục gốc của kho lưu trữ project. File thường bao gồm bởi các job, gọi là các **stages**. Mặc định 1 pipeline chạy với 3 stages là `build`, `test`, `deploy`. Với bất cứ push lên repository, GitLab sẽ nhìn vào file `.gitlab-ci.yml` và khởi động máy Runner thực thi công việc theo nội dung file.

Bởi vì `.gitlab-ci.yml` nằm trong kho lưu trữ và kiểm soát theo phiên bản, mỗi 1 lần push đều có thể kích hoạt pipeline CI nên yêu cầu có branch riêng phục vụ cho công việc liên quan đến quá trình CI này.

### 2. Cấu hình công việc với `.gitlab-ci.yml`
Từ phiên bản 7.12, GitLab CI sử dụng tệp [YAML](https://en.wikipedia.org/wiki/YAML) ( `.gitlab-ci.yml`) cho cấu hình dự án. YAML là 1 định dạng dữ liệu trung gian được thiết kế để người dùng và các ngôn ngữ lập trình cùng hiểu được, tương tự JSON, XML. File YAML dùng để chứa đựng thông tin ở dạng text, thường dùng cho các file config, lưu giá trị hằng, …

Khi chỉnh sửa file `.gitlab-ci.yml`, bạn có thể xác thực tệp đó bằng công cụ [CI Lint](https://docs.gitlab.com/ee/ci/lint.html) tool.

#### 2.1. Jobs
File `.gitlab-ci.yml` định nghĩa một tập hợp các `jobs` với ràng buộc khi nào chúng chạy. Mỗi 1 jobs phải luôn bắt buộc phải chứa mệnh đề `script`.
Example:
```bash
job1:
  script: 
	  - "ls -l /etc/"
	  - echo "Job done"
job2:
  script: 
	  - echo "Build app succesfull"
```
Ví dụ trên là cấu hình CI/CD đơn giản nhất có thể với hai công việc riêng biệt, trong đó mỗi công việc thực thi một vài lệnh shell đơn giản. Tất nhiên, một lệnh có thể thực thi trực tiếp hoặc chạy tập lệnh (makefile, .sh, ...).

Công việc được Runner chọn và thực hiện tuần tự trong môi trường của Runner. Điều quan trọng là mỗi công việc được thực hiện độc lập với nhau. Tên công việc được đặt là duy nhất và không trùng với tên các từ khóa **keyword**.

#### 2.2. Keywords
Cấu hình GitLab CI/CD pipeline bao gồm 3 nhóm từ khóa sau:

* **Global keywords**:

| Từ khóa		|Miêu tả                      |
|---------------|-----------------------------|
|`default`		|Xác định giá trị mặc định |
|`include`		|Nhập cấu hình từ các tệp YAML khác. |
|`stages`		|Tên và thứ tự của các giai đoạn trong 1 pipeline |
|`variable`		|Định nghĩa các biến cho tất cả các job trong 1 pipeline |
|`workflow`		|Kiểm soát những loại pipeline có thể chạy |


* **Header keywords**:

| Từ khóa		|Miêu tả                      |
|---------------|-----------------------------|
|`spec`			|Xác định thông số kỹ thuật cho các tệp cấu hình bên ngoài |


* **Job keywords**:

| Từ khóa		|Miêu tả                      |
|---------------|-----------------------------|
|`script`		|Định nghĩa tập lệnh shell được thực thi bởi Runner |
|`image`		|Chỉ định sử dụng docker image |
|`services`		|Chỉ định sử dụng docker services |
|`stage`		|Định nghĩa 1 job stage (mặc định: `test`) |
|`variables`	|Xác định các biến ở các cấp độ công việc |
|`only`			|Xác định danh sách các tham chiếu điều kiện của git để job được khởi chạy |
|`except`		|Xác định danh sách các tham chiếu điều kiện của git để job không được khởi chạy |
|`tags`			|Xác định danh sách các tag sử dụng chọn máy Runner chạy |
|`allow_failure`|Cho phép job thất bại, không góp phần vào trạng thái commit (tức là bỏ qua job thất bại) |
|`dependencies`	|Xác định các job khác mà job hiện tại phụ thuộc vào để có thể chuyển các output giữa chúng |
|`when`			|Xác định khi nào job chạy. Có thể là `on_success`, `on_failure`, `always` hoặc `manual` |
|`artifacts`	|xác định danh sách job artifacts |
|`cache`		|Xác định danh sách các tệp sẽ được lưu vào bộ đệm giữa các lần chạy tiếp theo |
|`before_script`|Ghi đè một tập lệnh shell được thực thi trước công việc |
|`after_script`	|Ghi đè một tập lệnh shell được thực thi sau công việc |
|`environment`	|Xác định tên của môi trường mà job thực thi |
|`coverage`		|Xác định cài đặt phạm vi code cho một job nhất định |
|`retry	`		|Xác định số lần job có thể được tự động chạy lại trong trường hợp thất bại |
|...			|...|

#### 2.3. Global keywords
Một số từ khóa không được xác định trong job. Những từ khóa này kiểm soát hành vi của pipeline hoặc nhập cấu hình pipeline bổ sung.

### `default`
Đặt mặt định chung cho 1 số từ khóa. Mỗi từ khóa mặc định sẽ được sao chép vào mọi job nếu chưa định nghĩa. Nếu job đã định nghĩa từ khóa đó thì từ khóa mặc định không được sử dụng.
**Ví dụ từ khóa  `default`**:

```bash
default:
  image: ruby:3.0
  retry: 2

rspec:
  script: bundle exec rspec

rspec 2.7:
  image: ruby:2.7
  script: bundle exec rspec

```

### `include`
Sử dụng `include` để thêm các tệp YAML bên ngoài trong cấu hình CI/CD của bạn. Nó dùng để chia một tệp `.gitlab-ci.yml` dài thành nhiều tệp để tăng khả năng đọc hoặc giảm sự trùng lặp của cùng một cấu hình ở nhiều nơi. Có thể lưu trữ các tệp mẫu trong kho lưu trữ trung tâm và đưa chúng vào các dự án.

File  `include`  là:
-   Được hợp nhất với những thứ trong file  `.gitlab-ci.yml` .
-   Luôn được evaluate trước rồi mới hợp nhất với nội dung của tệp `.gitlab-ci.yml`, bất kể vị trí của từ khóa `include`.

Các từ khóa subkeys của `include` bao gồm:
-   [`include:component`](https://docs.gitlab.com/ee/ci/yaml/#includecomponent)
-   [`include:local`](https://docs.gitlab.com/ee/ci/yaml/#includelocal)
-   [`include:project`](https://docs.gitlab.com/ee/ci/yaml/#includeproject)
-   [`include:remote`](https://docs.gitlab.com/ee/ci/yaml/#includeremote)
-   [`include:template`](https://docs.gitlab.com/ee/ci/yaml/#includetemplate)

Các tùy chọn thêm bao gồm:
-   [`include:inputs`](https://docs.gitlab.com/ee/ci/yaml/#includeinputs)
-   [`include:rules`](https://docs.gitlab.com/ee/ci/yaml/includes.html#use-rules-with-include)


### `stages`
Sử dụng `stages` để xác định các giai đoạn chứa các nhóm job cụ thể. Sử dụng `stage` trong một job để định cấu hình công việc sẽ chạy trong một giai đoạn cụ thể.

Nếu  `stages`  không được định nghĩa trong file  `.gitlab-ci.yml` , các stages mặc định là:

-   [`.pre`](https://docs.gitlab.com/ee/ci/yaml/#stage-pre)
-   `build`
-   `test`
-   `deploy`
-   [`.post`](https://docs.gitlab.com/ee/ci/yaml/#stage-post)

Thứ tự của các mục trong `stages`  xác định thức tự thực hiện các công việc:

-   Các job trong cùng 1 stage được chạy song song.
-   Các job trong stage kế tiếp chạy sau khi các job từ giai đoạn trước hoàn thành thành công.

Nếu 1 pipeline chỉ có các job ở các giai đoạn `.pre` or `.post`, nó sẽ không chạy. Phải có ít nhất một công việc khác ở giai đoạn khác.

**Ví dụ cho từ khóa  `stages`**:
```bash
stages:
  - build
  - test
  - deploy
```
### `workflow`

Sử dụng  `workflow`  để điều khiển hành vi của pipeline. Đọc thêm tại đây. [Link](https://docs.gitlab.com/ee/ci/yaml/workflow.html)


#### 2.4. Header keywords
Một số từ khóa phải được xác định trong phần tiêu đề của tệp cấu hình YAML. Tiêu đề phải ở đầu tệp, tách biệt với phần còn lại của cấu hình bằng `---`.

### `spec`

Thêm phần  `spec`  vào phần tiêu đề của file YAML để cấu hình hành vi của luồng thực thi pipeline khi một cấu hình khác được thêm vào quy trình với từ khóa  `include`.

**Ví dụ của từ khóa `spec`**:

```bash
spec:
  inputs:
    environment:
    job-stage:
---

scan-website:
  stage: $[[ inputs.job-stage ]]
  script: ./scan-website $[[ inputs.environment ]]
```



#### 2.5. Job keywords
Các từ khóa tiếp theo được sử dụng xác định các chức năng trong phạm vi 1 job.


### `stage`
Sử dụng `stage` để xác định giai đoạn nào một job sẽ chạy. Các job trong cùng một stage có thể thực thi song song. Nếu stage không được xác định, job sẽ sử dụng stage `test` theo mặc định.

**Ví dụ từ khóa  `stage`**:

```bash
stages:
  - build
  - test
  - deploy

job1:
  stage: build
  script:
    - echo "This job compiles code."

job2:
  stage: deploy
  script:
    - echo "This job deploys the code. It runs when the test stage completes."
  environment: production
```


#### `stage: .pre`	

Sử dụng  stage `.pre`  để thực hiện một công việc khi bắt đầu một pipeline.  `.pre`  luôn chạy đầu tiên, các giai đoạn do người dùng xác định sẽ thực thi sau `.pre`. Không cần phải định nghĩa  `.pre`  trong  `stages`.

#### `stage: .post`

Sử dụng  stage  `.post`  để thực hiện một công việc ở cuối một pipeline.  `.post`  luôn chạy cuối cùng, các giai đoạn do người dùng xác định sẽ thực thi trước   `.post`. Không cần phải định nghĩa  `.post`  in  `stages`.

### `script`
Sử dụng `script`  để chỉ định các lệnh để máy Runner thực thi. Tất cả các job ngoại trừ `trigger` đều yêu cầu từ khóa `script`.

**Ví dụ từ khóa `script`**:
```bash
job1:
  script: "bundle exec rspec"

job2:
  script:
    - uname -a
    - bundle exec rspec
```

### `only` và `except`
`only` và `except` là hai tham số đặt các điều kiện để giới hạn khi job được tạo:
1. `only` xác định tên của các branch và tag mà job sẽ chạy.
2. `except` xác định tên của các branch và tag mà job **không** chạy.

Ngoài ra, `only` và `except` cho phép sử dụng các từ khóa đặc biệt:
| Từ khóa		|Miêu tả                      |
|---------------|-----------------------------|
|`branches`		|Tên nhánh được push |
|`tags`			|Tên tag được push |
|`api`			|Khi pipeline được kích hoạt bởi API pipeline thứ hai (không phải API kích hoạt). |
|`pushes`		|Pipeline được kích hoạt bởi thao tác git push của người dùng |
|...			|...|


### `tags`
Sử dụng `tags` để chọn một Runner cụ thể với tag tương ứng từ danh sách tất cả các Runner có sẵn cho project. 

Khi đăng ký 1 Runner, Runner có thể được chỉ định tên tag, ví dụ `linux-arm64`, `postgres`, hay `development`. Để nhận và chạy một công việc, Runner phải được gán tag được liệt kê trong công việc.

**Ví dụ từ khóa`tags`**:
```bash
job:
  tags:
    - ruby
    - postgres
```

### `allow_failure`
`allow_failure` được sử dụng khi bạn muốn cho phép một job thất bại mà không ảnh hưởng đến phần còn lại của pipeline CI. Job thất bại không đóng góp vào trạng thái kết quả.
**Ví dụ từ khóa`allow_failure`**:
```bash
job1:
  stage: test
  script:
  - execute_script_that_will_fail
  allow_failure: true

job2:
  stage: test
  script:
  - execute_script_that_will_succeed

job3:
  stage: deploy
  script:
  - deploy_to_staging

```
Ở ví dụ bên trên, `job1` và `job2` sẽ chạy song song, nhưng có `job1` fail, stage tiếp theo vẫn sẽ chạy, bởi vì job1 được đánh dấu là `allow_failure: true`.


### `when`
Sử dụng `when` để cấu hình các điều kiện khi job thực thi. Nếu không được xác định trong 1 job, giá trị mặc đinh sẽ là `when: on_success`.

`when` có thể được cài đặt 1 trong các giá trị dưới đây:
-   `on_success`  (default): Chỉ chạy job khi không có job nào ở các stage trước bị lỗi hoặc có  `allow_failure: true`.
-   `on_failure`: Chỉ chạy job khi có ít nhất 1 job ở stage trước bị lỗi.  Job ở stage trước với  `allow_failure: true`  luôn được coi là thành công.
-   `never`: Không thực hiện công việc bất kể trạng thái công việc ở các stage trước. Chỉ có thể được sử dụng trong phần `rules` hoặc  `workflow: rules`.
-   `always`: Chạy công việc bất kể trạng thái của công việc ở các stage trước. Chỉ có thể được sử dụng trong phần `workflow:rules`.
-   `manual`: Chạy job khi được kích hoạt thủ công.
-   `delayed`:  Trì hoãn việc thực hiện một job trong một khoảng thời gian nhất định.

### `retry`
Sử dụng `retry` để định cấu hình số lần job được thử lại nếu nó thất bại. Nếu không định, mặc định là `0` và job đó không được chạy lại.

Theo mặc định, tất cả các loại lỗi đều khiến công việc phải được thử lại. Sử dụng `retry:when` hoặc `retry:exit_codes` để lựa chọn nhưng lỗi, mã lỗi nào cần chạy lại.


**Ví dụ từ khóa  `retry`**:
```bash
test:
  script: rspec
  retry: 2

test_advanced:
  script:
    - echo "Run a script that results in exit code 137."
    - exit 137
  retry:
    max: 2
    when: runner_system_failure
    exit_codes: 137
```


#### 2.6. Các keyword khác
Các keyword khác với các chức năng khác nhau, tham khảo ở [CI/CD YAML syntax reference](https://docs.gitlab.com/ee/ci/yaml/index.html)

Ví dụ nội dung đơn giản 1 file `.gitlab-ci.yml` cho iOS app build: 
```bash
stages:
	- build
	- test
	- archive
	- deploy

build_project:
	stage: build
	script:
		- xcodebuild clean -project ProjectName.xcodeproj -scheme SchemeName | xcpretty
		- xcodebuild test -project ProjectName.xcodeproj -scheme SchemeName -destination 'platform=iOS Simulator,name=iPhone 8,OS=11.3' | xcpretty -s

	tags:
		- ios_11-3
		- xcode_9-3
		- macos_10-13

archive_project:
	stage: archive
	script:
		- xcodebuild clean archive -archivePath build/ProjectName -scheme SchemeName
		- xcodebuild -exportArchive -exportFormat ipa -archivePath "build/ProjectName.xcarchive"  -exportPath "build/ProjectName.ipa" -exportProvisioningProfile "ProvisioningProfileName"
	rules:
		- if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
	artifacts:
	paths:
		- build/ProjectName.ipa
	tags:
		- ios_11-3
		- xcode_9-3
		- macos_10-13

deploy:
	stage: deploy
	script: echo "Define your deployment script!"
	environment: production
 
```
<!-- End of part I -->

<a id="part-two"></a>
## II. Configuring Runner

Trong GitLab, **Runner** thực hiện các công việc mà bạn xác định trong file `.gitlab-ci.yml`. Runner có thể là một máy ảo, một VPS, một máy vật lý cơ bản, một container docker hoặc thậm chí là một cụm container. GitLab và Runner giao tiếp thông qua API, vì vậy yêu cầu duy nhất là máy Runner có quyền truy cập Internet.

Máy Runner có thể dành riêng cho một dự án nhất định hoặc phục vụ nhiều dự án trong GitLab. Nếu nó phục vụ tất cả các dự án thì nó được gọi là **Shared Runner**.

Để tìm hiểu xem có Runner nào được chỉ định cho dự án của bạn hay không, truy cập 
**Settings ➔ CI/CD**. 

2 bước cài đặt để Runner hoạt động:
1. Cài đặt Runner trên máy
2. Cấu hình máy Runner

## 1. Cài đặt Runner
> **Note**: Tài liệu này chỉ dành cho việc cài đặt và cấu hình Runner trên máy macOS thực hiện công việc build app iOS. Các nền tảng khác vui lòng tham khảo tại tại [Install GitLab Runner](https://docs.gitlab.com/runner/install/#install-gitlab-runner "Permalink")

GitLab Runner có thể được cài đặt và cập nhật trên macOS.
1.  Download the binary for your system:
    
    -   Đối với máy tính chạy CPU Intel:
        
        ``` bash
        sudo curl --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-amd64"
        ```
        
    -    Đối với máy tính chạy CPU Apple Silicon:
		    ```bash
			sudo curl --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-arm64"
			```
        
        
2.  Cấp quyền thực thi:
    
    ```bash
    sudo chmod +x /usr/local/bin/gitlab-runner
    ```

3. Chạy Runner:
Mở ứng dụng Terminal. Chuyển qua tài khoản người dùng hiện tại, thao tác trên giao diện hoặc gõ lệnh: 
	```bash
	su - <username>
	```
	Cài đặt GitLab Runner như 1 dịch vụ và khởi chạy nó:
	```bash
	gitlab-runner install
	gitlab-runner start
	```
4. Khởi động lại máy
5. (Optional) Check trạng thái của GitLab Runner bằng lệnh:
	```bash
	gitlab-runner status
	```
6. (Optional) Dừng dịch vụ:
	```bash
	gitlab-runner stop
	```

## 2. Cấu hình Runner

### 2.1. Shared, specific và group
Runner có thể được đăng ký dưới 3 loại là: shared, specific và group.

 - **Shared Runners**: hữu ích cho các công việc có yêu cầu tương tự nhau, sử dụng cho nhiều project trong nhóm hoặc trong cùng 1 phiên bản GitLab. Nó xử lý các công việc bằng cách dùng hành đợi sử dụng hợp lý ([fair usage queue](https://git.sohacorp.vn/help/ci/runners/README.md#how-shared-runners-pick-jobs)). Chỉ có thể đăng ký Shared Runners nếu có quyền truy cập quản trị viên vào phiên bản GitLab.
 - **Specific Runners**: rất hữu ích cho các công việc có yêu cầu đặc biệt hoặc cho các dự án có nhu cầu cụ thể. Thông thường, các Specific Runner được sử dụng cho một dự án tại một thời điểm. Việc sử dụng `tags` có thể hữu ích trong trường hợp này. Specific Runner xử lý công việc theo hàng đợi *FIFO*. 
 - **Group Runners**: sử dụng khi có nhiều dự án trong 1 nhóm và muốn tất cả dự án trong nhóm đều có thể truy cập vào 1 tập hợp các Runner. Group Runners xử lý công việc theo hàng đợi *FIFO*. 
 
Specific Runners có thể được thiết lập để nhiều dự án sử dụng. Sự khác biệt với Shared Runner là phải kích hoạt rõ ràng từng dự án để Runner có thể thực hiện các công việc của mình. Specific Runners không được chia sẻ tự động với các dự án forked. Forked repository sẽ sao chép cài đặt CI (jobs, allow shared, ...).

### 2.2. Đăng ký Specific Runner
Việc đăng ký một **Specific Runner** có thể được thực hiện theo hai cách:

 - Tạo Runner bằng mã token
 - Chuyển đổi Shared Runner sang Specific Runner (1 chiều, chỉ quyền admin)

#### Đăng ký Specific Runner bằng token
Để tạo một Specific Runner mà không có quyền quản trị đối với dự án, hãy truy cập dự án mà bạn muốn cho Runner hoạt động trong GitLab. Làm theo các bước sau đây

 1. Vào **Settings > CI/CD** để lấy được mã CI/CD token
 2. Mở terminal, chạy lệnh:
	```bash
	gitlab-runner register
	```
3. Nhập GitLab URL:
    
    -  Đối với Runner trên GitLab tự quản lý, sử dụng URL cho phiên bản GitLab đó. Ví dụ, nếu dự án lưu trữ ở địa chỉ `https://git.sohacorp.vn/tunguyenvan/Telegrams_5.1`, URL của phiên bản GitLab sẽ là `https://git.sohacorp.vn/`.
    - Đối với Runner trên GitLab.com, URL là `https://gitlab.com`.
  4. Nhập mã token đăng ký Runner
  5. Nhập tên cho Runner
  6. Nhập type thực thi cho Runner. Để xác định được máy Runner phù hợp với loại thực thi vào, tham khảo ở [Link](https://docs.gitlab.com/runner/executors/index.html). Ở đây đơn giản là nhập `shell` để có thể chạy các job bằng các lệnh.

Một cách tương tự để đăng ký Runner là sử dụng chế độ không tương tác ([non-interactive mode](https://docs.gitlab.com/runner/commands/index.html#non-interactive-registration)) khi cung cấp các tham số trên 1 lệnh đăng ký:
```bash
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "$RUNNER_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \  
  --tag-list  "docker,aws"  \  
  --run-untagged="true"  \  
  --locked="true"  \
  --description "docker-runner"
```

Để kiểm tra trạng thái đăng ký có thành công hay chưa, sử dụng lệnh `gitlab-runner status` hoặc kiểm tra trên trang URL của dự án. Vào **Settings > CI / CD > Runner Settings** để xem các Runner có trong dự án như hình dưới đây.
<!-- Chèn ảnh cap màn hình vào đây -->

<img src="./image/specific-runners.png"/>


#### Khóa Specific Runner không được kích hoạt cho các dự án khác
Một Specific Runner có thể được chỉ định chạy cho một dự án và không bị kích hoạt bởi 1 dự án khác. Cài đặt này có thể được thực hiện ngay tại bước đăng ký Runner bằng tham số `--locked="true"` và có thể được thay đổi sau đó trong cài đặt của mỗi Runner.

 1. Từ Project truy vập **Settings > CI/CD**
 2. Tìm Runner muốn lock/unlock và đảm bảo rằng nó được enable
 3. Bấm vào nút bút chì bên cạnh tên Runner
 4. Tích chọn vào **Lock to current projects**
 5. Bấm vào **Save changes** để những thay đổi có hiệu lực


#### Protected Runners
Runner có thể được cài đặt bảo vệ khỏi tiết lộ thông tin nhạy cảm. Bất cứ khi nào Runner ở trạng thái `protected`, Runner chỉ chọn các công việc được tạo trên các `protected branch` hoặc `protected tag` và bỏ qua các job khác. Các bước cài đặt như bên dưới.

 1. Từ Project truy vập **Settings > CI/CD**
 2. Tìm Runner muốn protected/unprotected và đảm bảo rằng nó được enable
 3. Bấm vào nút bút chì bên cạnh tên Runner
 4. Tích chọn vào **Protected**
 5. Bấm vào **Save changes** để những thay đổi có hiệu lực

<img src="./image/protected_runners.png"/>

### 2.3. Đăng ký Shared Runner và Group Runner
Để đăng ký Shared Runner và Group Runner, tham khảo thêm tại [Registering runners](https://docs.gitlab.com/runner/register/).


### 2.4. Cài đặt máy Runner chạy macOS thực hiện công việc build/deploy app iOS
Khi đã đăng ký máy Runner chạy trên macOS, bước tiếp theo là thiết lập cấu hình để build hoặc deploy app iOS. Các điều kiện cần đó là:
-   Tài khoán Apple Developer -  [https://developer.apple.com/](https://developer.apple.com/)
-   Ruby và công cụ dòng lệnh XCode được cài đặt trên máy Runner [https://docs.fastlane.tools/getting-started/ios/setup](https://docs.fastlane.tools/getting-started/ios/setup/)

Có 2 cách để build hoặc deploy app iOS bằng các lệnh Shell như sau:

1. Sử dụng công cụ dòng lệnh Xcode là `xcodebuild`
		
	 - Đọc tài liệu về `xcodebuild` bằng lệnh:
		```bash
		man xcodebuild
		```
	 - Lệnh build project :
		```bash
		xcodebuild clean -project ProjectName.xcodeproj -scheme SchemeName | xcpretty
		xcodebuild build -project ProjectName.xcodeproj -scheme SchemeName -destination 'platform=iOS Simulator,name=iPhone 8,OS=11.3' | xcpretty -s
		```
	 - Lệnh archive project :
		```bash
		xcodebuild clean archive -archivePath build/ProjectName -scheme SchemeName
		xcodebuild -exportArchive -exportFormat ipa -archivePath "build/ProjectName.xcarchive" -exportPath "build/ProjectName.ipa" -exportProvisioningProfile "ProvisioningProfileName"
		```

	Tham khảo thêm tài liệu [Building from the Command Line with Xcode FAQ](https://developer.apple.com/library/archive/technotes/tn2339/_index.html)


2. Sử dụng thư viện **fastlane**

    Dự án Demo để tham khảo: [https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo).

    - Cài đặt **fastlane**:

      Tạo một file trong kho lưu trữ của dự án có tên là `Gemfile`, dán nội dung sau vào file:
        ```bash
        source "https://rubygems.org"
        gem "fastlane"
        ```

      Sau đó vào Terminal ở vị trí dự án, chạy lệnh: 
        ```bash
        bundle install
        ```
		
	- Khởi tạo **fastlane**:

		Chạy lệnh sau trên Terminal, chọn Option 2 cho target **Test Flight**:
		```bash
		bundle exec fastlane init
		```

		Sau đó nhập các thông tin như là tài khoản Apple Developer để hoàn thành đăng ký. Công cụ sẽ tạo sẵn folder `fastlane` với 2 file `Appfile` and `Fastfile`.
		
		![Initialize Fastlane](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/fastlane-init.png)
    
	- Khởi tạo **fastlane match**:

		Chạy lệnh và chọn Option 4:
		```bash
		bundle exec fastlane match init
		```
		![Initialize fastlane Match](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/match-init.png)
Để biết thêm thông tin về **fastlane match**, đọc thêm tài liệu [https://docs.fastlane.tools/actions/match/](https://docs.fastlane.tools/actions/match/)

	- Tạo **project access token**:

		Vào trong Profile của tài khoản GitLab để lấy được GitLab Access Token. Sau đó chạy lệnh sau để định cấu hình **fastlane** để sử dụng Access Token khi thực hiện các yêu cầu cho dự án:
		```bash
		export PRIVATE_TOKEN=YOUR_NEW_TOKEN
		```

	- Tạo **signing certificates**:

		Giờ đây **fastlane match** được cấu hình, chúng ta có thể sử dụng nó để tạo **signing certificates** và **provisioning profiles** cho ứng dụng và tải chúng lên GitLab.
		> **Note**: Nếu đã có những file này cho ứng dụng của mình, hãy xem hướng dẫn trong blog này về cách sử dụng **fastlane** để nhập các file: [/blog/2022/10/03/mobile-devops-with-gitlab-part-3-code-signing-for-ios-with-gitlab-and-fastlane/](https://about.gitlab.com/blog/2022/10/03/mobile-devops-with-gitlab-part-3-code-signing-for-ios-with-gitlab-and-fastlane/).

		Chạy lệnh sau từ Terminal để tạo file **signing certificates** và upload lên GitLab:
		```bash
		bundle exec fastlane match development
		```

		Khi lệnh này hoàn tất, hãy truy cập **Settings > CI/CD** trong dự án và cuộn xuống phần **Secure Files** để xem các tệp vừa được tạo và thêm vào dự án.
		
		Làm tương tự với tạo file **Appstore code signing** bằng lệnh:
		```bash
		bundle exec fastlane match appstore
		```

	- Cập nhật cấu hình **Xcode**:

		Trên Project trên **Xcode**, vào **Signing & Capabilities** và disable **Automatically managing signing**. Sau đó chọn **Provisioning profile** và **Signing certificate** phù hợp..

		![Configure Xcode Provisioning Profiles](https://about.gitlab.com/images/blogimages/2023-04-15-ios-cicd-with-gitlab/xcode.png)



	
⇒ Công việc còn lại là cấu hình file `.gitlab-ci.yml` để thực hiện các công việc với **fastlane**.

Ví dụ cấu hình file `.gitlab-ci.yml` chạy fastlane đơn giản: 
```bash
stages:
	- build
	- beta
cache:
	key:
		files:
			- Gemfile.lock
		paths:
			- vendor/bundle

build_ios:
	image: macos-13-xcode-14
	stage: build
	script:
		- bundle check --path vendor/bundle || bundle install --path vendor/bundle --jobs $(nproc)
		- bundle exec fastlane build
	tags: 
		- saas-macos-medium-m1

	beta_ios:
		image: macos-13-xcode-14
		stage: beta
		script:
			- bundle check --path vendor/bundle || bundle install --path vendor/bundle --jobs $(nproc)
			- bundle exec fastlane beta
		tags: 
			- saas-macos-medium-m1
		when:  manual
		allow_failure:  true
		only:
			refs:
				- main
```

> **Note**: Video hướng dẫn chi tiết tại: [Incubation Engineering - Mobile DevOps - Feb 2023 Showcase](https://www.youtube.com/watch?v=Ar8IsBgP1as)

<!-- End of part II -->


## Author
- Nguyễn Quang Hưng
- Facebook: https://www.facebook.com/hungnq.SoICT
- Email: nguyenquanghung.ktmt@gmail.com
- LinkedIn: https://www.linkedin.com/in/hungnq-soict
- Github: https://github.com/nguyenquanghungktmt

## References
* [Getting started with GitLab CI/CD](https://git.sohacorp.vn/help/ci/quick_start/README)
* [Configuration of your jobs with .gitlab-ci.yml](https://git.sohacorp.vn/help/ci/yaml/README.md)
* [GitLab CI/CD Examples](https://git.sohacorp.vn/help/ci/examples/README.md)
* [Setting up GitLab CI for iOS projects](https://about.gitlab.com/blog/2016/03/10/setting-up-gitlab-ci-for-ios-projects/)
* [Tutorial: iOS CI/CD with GitLab](https://about.gitlab.com/blog/2023/06/07/ios-cicd-with-gitlab/)
* [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/#step-3-define-your-pipelines)
* [CI/CD YAML syntax reference](https://docs.gitlab.com/ee/ci/yaml/)